# NLX - Haal Centraal BRK

![Architectural diagram of NLX + Haal Centraal](./nlx-haal-centraal.png)

This is an example how to use NLX together with the Haal Centraal BRK API and is based on [NLX Try Me](https://gitlab.com/commonground/nlx/nlx-try-me).

This example adds two new components:

- user-proxy: this sits before the NLX outway and authenticates user requests by a JWT token.
- service-proxy: this sits between NLX inway and the Haal Centraal BRK API. It adds the ability to provide an API key to the request in acceptance and authenticates with the Haal Centraal BRK API with a certificate in production.
