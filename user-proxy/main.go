package main

import (
	"encoding/json"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"

	"github.com/MicahParks/keyfunc"
	"github.com/golang-jwt/jwt/v4"
	"github.com/ory/oathkeeper/helper"
)

// NewProxy takes target host and creates a reverse proxy
func NewProxy(targetHost string) (*httputil.ReverseProxy, error) {
	url, err := url.Parse(targetHost)
	if err != nil {
		return nil, err
	}

	proxy := httputil.NewSingleHostReverseProxy(url)

	d := proxy.Director
	proxy.Director = func(r *http.Request) {
		d(r)

		target, err := url.Parse(os.Getenv(("UPSTREAM_URL")))
		if err != nil {
			log.Fatal(err)
		}

		r.Host = target.Host
	}

	return httputil.NewSingleHostReverseProxy(url), nil
}

// ProxyRequestHandler handles the http request using proxy
func ProxyRequestHandler(proxy *httputil.ReverseProxy) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		headers := w.Header()
		headers.Set("Access-Control-Allow-Origin", "*")
		headers.Set("Access-Control-Allow-Headers", "*")

		if r.Method == "OPTIONS" {
			return
		}

		// Create the JWKS from the resource at the given URL.
		jwks, err := keyfunc.Get(os.Getenv("JWKS_URL"), keyfunc.Options{})
		if err != nil {
			writeError(w, http.StatusUnauthorized, "Could not fetch JWKS from server")
			return
		}

		tokenFromRequest := helper.DefaultBearerTokenFromRequest(r)
		if tokenFromRequest == "" {
			writeError(w, http.StatusUnauthorized, "Could not find token in authorization header")
			return
		}

		parsedToken, err := jwt.Parse(tokenFromRequest, jwks.Keyfunc)
		if err != nil {
			writeError(w, http.StatusUnauthorized, "Could not parse token")
			return
		}

		if parsedToken.Valid {
			claims := parsedToken.Claims.(jwt.MapClaims)
			r.Header.Add("X-NLX-Request-User-Id", claims["email"].(string))
			proxy.ServeHTTP(w, r)
			return
		} else {
			writeError(w, http.StatusUnauthorized, "Could not validate token")
		}
	}
}

func main() {
	// initialize a reverse proxy and pass the actual backend server url here
	proxy, err := NewProxy(os.Getenv("UPSTREAM_URL"))
	if err != nil {
		panic(err)
	}

	// handle all requests to your server using the proxy
	http.HandleFunc("/", ProxyRequestHandler(proxy))
	log.Fatal(http.ListenAndServe(os.Getenv("LISTEN_ADDRESS"), nil))
}

func writeError(w http.ResponseWriter, statusCode int, message string) {
	resp := make(map[string]string)
	resp["message"] = message
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		log.Fatalf("Error happened in JSON marshal. Err: %s", err)
	}

	w.WriteHeader(statusCode)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonResp)
}
