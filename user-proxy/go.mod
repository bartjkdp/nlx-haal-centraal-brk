module gitlab.com/delta10/user-proxy

go 1.18

require (
	github.com/MicahParks/keyfunc v1.1.0 // indirect
	github.com/coreos/go-oidc/v3 v3.2.0 // indirect
	github.com/golang-jwt/jwt/v4 v4.4.2 // indirect
	github.com/golang/gddo v0.0.0-20190904175337-72a348e765d2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/magefile/mage v1.10.0 // indirect
	github.com/ory/herodot v0.8.4 // indirect
	github.com/ory/oathkeeper v0.39.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.8.0 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20220624214902-1bab6f366d9e // indirect
	golang.org/x/oauth2 v0.0.0-20220630143837-2104d58473e0 // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.28.0 // indirect
	gopkg.in/square/go-jose.v2 v2.5.1 // indirect
)
