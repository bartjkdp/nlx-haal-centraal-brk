#!/bin/bash 
set -o errexit
set -o pipefail
set -o nounset

JQ_VERSION="${JQ_VERSION:-1.6}"
CERTPORTAL_URL="${CERTPORTAL_URL:-https://certportal.demo.nlx.io}"
OUTPUT_PATH="${OUTPUT_PATH:-.}"

# Download jq
wget -O /usr/bin/jq https://github.com/stedolan/jq/releases/download/jq-${JQ_VERSION}/jq-linux64 && chmod +x /usr/bin/jq

# Organization cert
mkdir -p ${OUTPUT_PATH}/ca
mkdir -p ${OUTPUT_PATH}/certs

echo "Downloading NLX root cert"
curl "${CERTPORTAL_URL}/root.crt" -o ${OUTPUT_PATH}/ca/root.crt -s

echo "Downloaded NLX root cert: $(openssl x509 -in ${OUTPUT_PATH}/ca/root.crt -text | grep Subject:)"

echo "Generating organization cert"
echo -e "[req_distinguished_name]\nserialNumber=An optional organization serial number (OIN), will be generated for you if empty (eg, 12345678901234567890)" >> /etc/ssl/openssl.cnf
openssl req -utf8 -nodes -sha256 -newkey rsa:4096 \
    -keyout ${OUTPUT_PATH}/certs/org.key \
    -out ${OUTPUT_PATH}/certs/org.csr

echo "Signing organization cert via ${CERTPORTAL_URL}"
curl -s -X POST "${CERTPORTAL_URL}/api/request_certificate" \
   -H 'Content-Type: application/json' \
   -d "{\"csr\":$(cat ${OUTPUT_PATH}/certs/org.csr | jq -sR .)}" | jq -r '.certificate' > ${OUTPUT_PATH}/certs/org.crt

echo "Got signed cert from certportal: $(openssl x509 -in ${OUTPUT_PATH}/certs/org.crt -text | grep Subject:)"
